<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::resource('contact-details', ContactDetailsController::class);

Route::prefix('contact-details')->name('contact-details.')->group(function() {
    Route::get('', 'ContactDetailsController@index')->name('index');
    Route::post('', 'ContactDetailsController@store')->name('store');

    Route::prefix('{contact_details}')->group(function() {
        Route::get('', 'ContactDetailsController@show')->name('show');
        Route::match(['PUT', 'PATCH'], '', 'ContactDetailsController@update')->name('update');
        Route::delete('', 'ContactDetailsController@delete')->name('delete');
    });
});
