@component('mail::message')

Your contact details where successfully received.

Best regards,<br>
{{ config('app.name') }}

@endcomponent
