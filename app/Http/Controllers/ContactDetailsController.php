<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\ContactDetails;

use App\Http\Requests\SaveContactDetails;

use App\Http\Resources\ContactDetails as ContactDetailsCollection;

use App\Mail\ContactDetailsReceived;

use Mail;

class ContactDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ContactDetailsCollection::collection(ContactDetails::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\SaveContactDetails  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveContactDetails $request)
    {
        $contact_details = new ContactDetails;
        $contact_details->name      = $request->get('name');
        $contact_details->email     = $request->get('email');
        $contact_details->message   = $request->get('message');
        $contact_details->save();

        Mail::to($contact_details)->send(new ContactDetailsReceived);

        return new ContactDetailsCollection($contact_details);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ContactDetails $contact_details)
    {
        return new ContactDetailsCollection($contact_details);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\SaveContactDetails  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SaveContactDetails $request, ContactDetails $contact_details)
    {
        $contact_details->name      = $request->get('name');
        $contact_details->email     = $request->get('email');
        $contact_details->message   = $request->get('message');
        $contact_details->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(ContactDetails $contact_details)
    {
        $contact_details->delete();
    }
}
