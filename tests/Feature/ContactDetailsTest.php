<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ContactDetailsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test for a job post example that should pass validation.
     *
     * @return void
     */
    public function testPostContactDetails()
    {
        $headers = [ 'Accept' => 'application/json' ];
        $dataArray = [
            'name'      => 'Ervin Cfarku',
            'email'     => 'ervin@cfarku.com',
            'message'   => 'Sample message',
        ];

        $response = $this->withHeaders($headers)->postJson(route('api.contact-details.store'), $dataArray);

        $response->assertStatus(201)->assertJson(['data' => $dataArray]);
    }

    /**
     * A basic test for a job post example that shouldn't pass validation.
     *
     * @return void
     */
    public function testPostContactDetailsWithoutMessage()
    {
        $headers = [ 'Accept' => 'application/json' ];
        $dataArray = [
            'name'      => 'Ervin Cfarku',
            'email'     => 'ervin@cfarku.com',
        ];

        // Posting an incomplete amount of data to the API
        $response = $this->withHeaders($headers)->postJson(route('api.contact-details.store'), $dataArray);

        // The return code should be 422 to indicate an error with the data
        $response->assertStatus(422);
    }
}
